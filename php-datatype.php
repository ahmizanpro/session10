<?php
// PHP Data Type  - 8 types

/**
 * Scalar type is 4
 * 1.  Boolean 
 * 2. Integer
 * 3. String
 * 4. Float
 * **/ 


 //Boolean Datatype
 $myTestVar = true;
  var_dump($myTestVar); //by this function to grab details information about a particular variable.

echo "<br>";
  //Integer Datatype
 $myTestVar = 100;
 var_dump($myTestVar);

 echo "<br>";
  //Float Datatype
 $myTestVar = 100.50;
 var_dump($myTestVar);

 echo "<br>";
  //String Datatype
 $myTestVar = "100";
 var_dump($myTestVar);


//  Compound datatype ares 2 
/**
 * Array
 * Associative array
 * indexed array
 * Object Type 
 */

echo "<br>";

 
 //Example of Array Datatype
 $name = "Abul";
 $age = 25 ;
 $height = 5.10 ;

 echo $name." is ".$age." years old and height is ".$height." Inches."; //concatination of PHP by "."
 echo "<br>";

 //Indexed Array
 $person = array(
    array("X", 4, 4.8),
    array("Y", 2, 1.8),
    array("Z", 3, 2.8),
 );


//Associcative Array
$age = array(
    "Kuddus" => 45,
    "Zarina" => 30,
    "Moynar Ma" => 40,
    "Abul" => 66
);

echo $age["Moynar Ma"];

echo "<br>";


//Using foreach loop 
$person1 = array(
    "Kuddus" => array("Age" => 54, "Height" => 5.8),
    "Kuddus1" => array("Age" => 64, "Height" => 4.8),
    "Kuddus2" => array("Age" => 44, "Height" => 6.8),
    "Kuddus3" => array("Age" => 34, "Height" => 5.10),
);

echo $person["Kuddus2"]["Height"];

echo "<br>";

foreach($person1 as $key => $value){
    echo "<pre>";
    print_r($person1);
    echo "</pre>";
}

echo "<br>";
//Object type
class Person{
    public $name = "Abul";
    public $age = 35;
    public $height = 5.6;

    public $xyz = array();
}

$objPerson = new Person();
var_dump($objPerson);

// What is the difference between NULL and EMPTY